/*
 * button.h
 *
 *  Created on: Oct 20, 2020
 *      Author: przemek
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_

typedef enum {
	IDLE = 0,
	DEBOUNCE,  // 1
	PRESSED,  // 2
	REPEAT,  // 3
	RELEASE  // 4
} BUTTON_STATE;

typedef void(*pfnButtonPress)(void);

typedef struct button_struct {
	BUTTON_STATE   state;
	GPIO_TypeDef*  gpioPort;
	uint16_t       gpioPin;
	uint32_t       lastTick;
	uint32_t       timer;
	uint32_t       timerIdle;
	uint32_t       timerDebounce;
	uint32_t       timerPressed;
	uint32_t       timerRepeat;
	uint32_t       timerRelease;
	pfnButtonPress buttonPress;
	pfnButtonPress buttonLongPress;
	pfnButtonPress buttonRepeatPress;
	pfnButtonPress buttonRelease;
} TButton;

void buttonInitKey(TButton* key,
		           GPIO_TypeDef* gpioPort,
				   uint16_t gpioPin,
				   uint32_t timerIdle,
				   uint32_t timerDebounce,
				   uint32_t timerPressed,
				   uint32_t timerRepeat,
				   uint32_t timerRelease);

void buttonRegisterPressCallback(TButton* key, void* callback);
void buttonRegisterLongPressCallback(TButton* key, void* callback);
void buttonRegisterRepeatPressCallback(TButton* key, void* callback);
void buttonRegisterReleaseCallback(TButton* key, void* callback);

void buttonTask(TButton* key);

#endif /* INC_BUTTON_H_ */
