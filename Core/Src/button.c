/*
 * button.c
 *
 *  Created on: Oct 20, 2020
 *      Author: przemek
 */

#include "main.h"
#include "button.h"

TButton blueKey;

void buttonInitKey(TButton* key,
		           GPIO_TypeDef* gpioPort,
				   uint16_t gpioPin,
				   uint32_t timerIdle,
				   uint32_t timerDebounce,
				   uint32_t timerPressed,
				   uint32_t timerRepeat,
				   uint32_t timerRelease) {
	key->state = IDLE;
	key->gpioPort = gpioPort;
	key->gpioPin = gpioPin;
	key->timer = timerIdle;
	key->timerIdle = timerIdle;
	key->timerDebounce = timerDebounce;
	key->timerPressed = timerPressed;
	key->timerRepeat = timerRepeat;
	key->timerRelease = timerRelease;
}

void buttonRegisterPressCallback(TButton* key, void* callback) {
	key->buttonPress = callback;
}

void buttonRegisterLongPressCallback(TButton* key, void* callback) {
	key->buttonLongPress = callback;
}

void buttonRegisterRepeatPressCallback(TButton* key, void* callback) {
	key->buttonRepeatPress = callback;
}

void buttonRegisterReleaseCallback(TButton* key, void* callback) {
	key->buttonRelease = callback;
}

int buttonPressed(GPIO_TypeDef* gpioPort, uint16_t gpioPin) {
	return GPIO_PIN_RESET == HAL_GPIO_ReadPin(gpioPort, gpioPin);
}

void changeState(TButton* key, BUTTON_STATE state, uint32_t timer) {
	key->state = state;
	key->timer = timer;
	key->lastTick = HAL_GetTick();
}

int timerOk(TButton* key) {
	return (HAL_GetTick() - key->lastTick) > key->timer;
}

void buttonIdleRoutine(TButton* key) {
	if(timerOk(key)) {  // Debouncing
		if(buttonPressed(key->gpioPort, key->gpioPin)) {
			changeState(key, DEBOUNCE, key->timerDebounce);
		} else {
			changeState(key, IDLE, key->timerIdle);
		}
	}
}

void buttonDebounceRoutine(TButton* key) {
	if(buttonPressed(key->gpioPort, key->gpioPin)) {
		if(timerOk(key)) {  // Debouncing
			if(key->buttonPress != NULL) {
				key->buttonPress();
			}
			changeState(key, PRESSED, key->timerPressed);
		}
	} else {
		changeState(key, IDLE, key->timerIdle);
	}
}

void buttonPressedRoutine(TButton* key) {
	if(buttonPressed(key->gpioPort, key->gpioPin)) {
		if(timerOk(key)) {  // Debouncing
			if(key->buttonLongPress != NULL) {
				key->buttonLongPress();
			}
			changeState(key, REPEAT, key->timerRepeat);
		}
	} else {
		changeState(key, RELEASE, key->timerRelease);
	}
}

void buttonRepeatRoutine(TButton* key) {
	if(buttonPressed(key->gpioPort, key->gpioPin)) {
		if(timerOk(key)) {  // Debouncing
			if(key->buttonRepeatPress != NULL) {
				key->buttonRepeatPress();
			}
			changeState(key, REPEAT, key->timerRepeat);
		}
	} else {
		changeState(key, RELEASE, key->timerRelease);
	}
}

void buttonReleaseRoutine(TButton* key) {
	if(timerOk(key)) {  // Debouncing
		if(key->buttonRelease != NULL) {
			key->buttonRelease();
		}
		changeState(key, IDLE, key->timerIdle);
	}
}

void buttonTask(TButton* key) {
	switch(key->state) {
	case IDLE:
		buttonIdleRoutine(key);
		break;
	case DEBOUNCE:
		buttonDebounceRoutine(key);
		break;
	case PRESSED:
		buttonPressedRoutine(key);
		break;
	case REPEAT:
		buttonRepeatRoutine(key);
		break;
	case RELEASE:
		buttonReleaseRoutine(key);
		break;
	default:
		break;
	}
}
